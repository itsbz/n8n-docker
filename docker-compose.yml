version: '3.8'

x-shared: &shared
  restart: always
  env_file: .env
  links:
    - postgres
    - redis
  volumes:
    - /etc/localtime:/etc/localtime:ro
    - /etc/timezone:/etc/timezone:ro
    - ./data/n8n:/home/node/.n8n
  depends_on:
    redis:
      condition: service_healthy
    postgres:
      condition: service_healthy

services:
  postgres:
    image: postgres:15
    restart: always
    container_name: n8n-postgres
    env_file: .env
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro
      - ./data/db:/var/lib/postgresql/data
      - ./src/init-data.sh:/docker-entrypoint-initdb.d/init-data.sh
    healthcheck:
      test: ['CMD-SHELL', 'pg_isready -h localhost -U ${POSTGRES_USER} -d ${POSTGRES_DB}']
      interval: 5s
      timeout: 5s
      retries: 10

  redis:
    image: redis:alpine
    restart: always
    container_name: n8n-redis
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro
      - ./data/redis:/data
    healthcheck:
      test: ['CMD', 'redis-cli', 'ping']
      interval: 5s
      timeout: 5s
      retries: 10

  n8n:
    <<: *shared
    image: docker.n8n.io/n8nio/n8n
    container_name: n8n-app
    ports:
      - $APP_PORT:5678
    volumes:
      - ./data/n8n:/home/node/.n8n
    healthcheck:
      test: [ "CMD", "nc", "-vz", "localhost", "5678" ]
      interval: 5s
      timeout: 5s
      retries: 10

  n8n-worker:
    <<: *shared
    image: docker.n8n.io/n8nio/n8n
    container_name: n8n-worker
    command: worker --concurrency=2
    deploy:
      mode: replicated
      replicas: 1
    depends_on:
      - n8n
    healthcheck:
      test: [ "CMD", "nc", "-vz", "localhost", "5678" ]
      interval: 5s
      timeout: 5s
      retries: 10
    depends_on:
      - n8n
