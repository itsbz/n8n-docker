start: pull up clear

pull:
	@docker-compose pull

up:
	@docker-compose up -d

down:
	@docker-compose down

stop:
	@docker-compose stop

clear:
	@docker system prune -af --volumes

backup:
	sh ./backup.sh
