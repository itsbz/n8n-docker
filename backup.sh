#!/bin/bash

fname=backup-$(date +%Y-%m-%d).tar
docker exec -it n8n-postgres sh -c "fname=backup-$(date +%Y-%m-%d).tar && mkdir -p /var/lib/postgresql/data/backup && pg_dump -U n8nroot -F t n8n > /var/lib/postgresql/data/backup/$fname"
echo Backup was created and accessed in ./data/db/backup/$fname local directory
